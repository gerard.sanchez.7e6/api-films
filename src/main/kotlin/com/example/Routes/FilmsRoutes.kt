package com.example.Routes
import Films
import com.example.models.Comentari
import io.ktor.server.routing.*
import filmsStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*


fun Route.filmsRouting() {
    route("/films") {
        get {
            if (filmsStorage.isNotEmpty()) call.respond(filmsStorage)
            else call.respondText("No films found.", status = HttpStatusCode.OK)
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (film in filmsStorage) {
                if (film.id == id) return@get call.respond(film)
            }
            call.respondText(
                "Film with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        post {
            val customer = call.receive<Films>()
            filmsStorage.add(customer)
            call.respondText("Film stored correctly", status = HttpStatusCode.Created)
        }
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            val custToUpd = call.receive<Films>()
            for (film in filmsStorage) {
                if (film.id == id) {
                    film.name = custToUpd.name
                    film.year = custToUpd.year
                    film.genre = custToUpd.genre
                    film.director = custToUpd.director
                    return@put call.respondText(
                        "Film with id $id has been updated",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText(
                "Film with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in filmsStorage) {
                if (customer.id == id) {
                    filmsStorage.remove(customer)
                    return@delete call.respondText("Film removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText(
                "Film with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        route("{id?}/comments") {
            get {
                val id = call.parameters["id"]
                for (film in filmsStorage) {
                    if (film.id == id) {
                        if (film.comentari.isNotEmpty()) {
                            call.respond(film.comentari)
                        } else {
                            call.respondText(
                                "No comments found",
                                status = HttpStatusCode.OK
                            )
                        }
                    }
                }
            }
            post {
                val comment = call.receive<Comentari>()
                val id = call.parameters["id"]
                for (film in filmsStorage) {
                    if (film.id == id) {
                        film.comentari.add(comment)
                        call.respondText(
                            "Comment stored correctly",
                            status = HttpStatusCode.Created
                        )
                    }
                }
            }
        }
    }

}



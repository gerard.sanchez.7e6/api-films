
import com.example.models.Comentari
import kotlinx.serialization.Serializable

@Serializable
data class Films(
    val id: String,
    var name: String,
    var year: String,
    var genre: String,
    var director: String,
    var comentari: MutableList<Comentari>
)
val filmsStorage = mutableListOf<Films>()

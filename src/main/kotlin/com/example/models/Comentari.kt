package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Comentari(
    val id: String,
    var filmId: String,
    var comment: String,
    var date: String,
)

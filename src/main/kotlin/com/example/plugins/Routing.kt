package com.example.plugins


import com.example.Routes.filmsRouting
import io.ktor.server.routing.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        filmsRouting()

    }
}
